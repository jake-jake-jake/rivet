# Tests for Rivet

<!-- RIVETS -->
[A link]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/sample-sha-file#L3> (b53a6c79c8e251e8192e5517f388cb16b6dfbabb:c1f08421776775baac9b2345894aa227fed0524f)
[Another link]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/tests/sample-sha-file#L1-L4> (b53a6c79c8e251e8192e5517f388cb16b6dfbabb:6585836c519fc09b76ade5a3c1d2e16c0be3026f)
[A broken link]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/tests/sample-sha-file#L100> (b53a6c79c8e251e8192e5517f388cb16b6dfbabb:abadon)

This references [a link][A link]. 
This references [another link][Another link]. 
This references [a broken link][A broken link].

