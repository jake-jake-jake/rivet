extern crate rivet;

#[cfg(test)]
mod tests {
    use std::{env, io::Result as ioResult};
    use std::path::PathBuf;
    use rivet::*;

    // assume that cargo is testing from the root directory of the package
    #[test]
    fn test_get_repository() -> ioResult<()>  {
        let path = env::current_dir()?;
        let _repo = get_repository(path);
        Ok(())
    }

    // ensure that we panic if we can't find a repo
    #[test]
    #[should_panic]
    fn test_get_repository_panic() {
        let no_repo_dir = PathBuf::from("/var/lib");
        let _repo2 = get_repository(no_repo_dir);
    }

    // assume that cargo is testing from the root directory of the package
    // and can find the README.md for it.
    #[test]
    fn test_get_doc_files() -> ioResult<()>  {
        let path = env::current_dir()?;
        let docs = get_doc_files(&path);
        let paths: Vec<PathBuf> = docs.iter().map(|i| i.path()).map(|p| p.to_path_buf()).collect();
        let mut readme_path = PathBuf::from(&env::current_dir()?);
        readme_path.push("README.md");
        assert!(paths.contains(&readme_path));
        Ok(())
    }

    // test that we properly extract rivets and indices from given string data;
    #[test]
    fn test_get_rivet_holes() -> ioResult<()>  {
        let mut some_contents = String::from("There is no rivet in this file.\n");
        let rivets = get_rivet_holes(&some_contents);
        assert!(rivets.is_empty());

        // insert some rivets into contents and ensure we're getting the rivet
        some_contents.push_str("<!-- RIVETS\n[A rivet]: <a link>\n");
        let rivets = get_rivet_holes(&some_contents);
        assert_eq!(rivets.len(), 1);
        assert_eq!(rivets[0], (2, "[A rivet]: <a link>"));

        // insert some more lines and ensure that we stop tracking rivets
        some_contents.push_str("Another line of text.\n[not a rivet]\n");
        let rivets = get_rivet_holes(&some_contents);
        assert_eq!(rivets.len(), 1);

        // insert some more lines and ensure that we stop tracking rivets
        some_contents.push_str("<!-- RIVETS -->\n[Another rivet]\n");
        let rivets = get_rivet_holes(&some_contents);
        assert_eq!(rivets.len(), 2);
        assert_eq!(rivets[0], (2, "[A rivet]: <a link>"));
        assert_eq!(rivets[1], (6, "[Another rivet]"));
        Ok(())
    }

    // test that we properly attach refs
    #[test]
    fn test_set_rivets() -> ioResult<()> {
        let doc_contents = String::from("# A Sample Doc\n\
                                        <!-- RIVETS -->\n\
                                        [Code Sample]: <./src/lib.rs>\n\
                                        [Another Sample]: <./a_bad_ref>\n\
                                        [A Sample with Comment]: <./a_bad_ref>\n\
                                        \n\
                                        The doc goes on...");
        // [Already riveted]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L28> (A title) {stuff}\n\
        let rivet_holes = get_rivet_holes(&doc_contents);
        let attached_rivets = set_rivets(rivet_holes);
        println!("Rivets attached! {:?}", attached_rivets);
        Ok(())
    }

    // test that rivets properly make links for a repo origin
    #[test]
    fn test_rivet_make_link() -> ioResult<()> {
        // test without line number links
        let r = Rivet {
            name: "A Link",
            doc_line: 2,
            code_ref: "./src/main.rs",
            status: RivetStatus::UnAttached,
            ..Default::default()
        };

        // test gitlab
        let git_origin = "git@gitlab.com:jake-jake-jake/rivet.git";
        let http_origin = "https://gitlab.com/jake-jake-jake/rivet.git";
        let expected = String::from("https://gitlab.com/jake-jake-jake/rivet/blob/master/src/main.rs");
        assert_eq!(r.make_link(git_origin), Ok(expected));
        let expected = String::from("https://gitlab.com/jake-jake-jake/rivet/blob/master/src/main.rs");
        assert_eq!(r.make_link(http_origin), Ok(expected));

        // test arbitrarty domain
        let git_origin = "git@some.very.special.domain:a-username-here/repo.git";
        let http_origin = "https://some.very.special.domain/a-username-here/repo.git";
        let expected = String::from("https://some.very.special.domain/a-username-here/repo/blob/master/src/main.rs");
        assert_eq!(r.make_link(git_origin), Ok(expected));
        let expected = String::from("https://some.very.special.domain/a-username-here/repo/blob/master/src/main.rs");
        assert_eq!(r.make_link(http_origin), Ok(expected));

        // test without `./` in code_ref
        let r = Rivet {
            name: "A Link",
            doc_line: 2,
            code_ref: "src/main.rs",
            status: RivetStatus::UnAttached,
            ..Default::default()
        };

        let git_origin = "git@gitlab.com:jake-jake-jake/rivet.git";
        let http_origin = "https://gitlab.com/jake-jake-jake/rivet.git";
        let expected = String::from("https://gitlab.com/jake-jake-jake/rivet/blob/master/src/main.rs");
        assert_eq!(r.make_link(git_origin), Ok(expected));
        let expected = String::from("https://gitlab.com/jake-jake-jake/rivet/blob/master/src/main.rs");
        assert_eq!(r.make_link(http_origin), Ok(expected));

        // test with line numbers
        let r = Rivet {
            name: "A Link",
            doc_line: 2,
            code_ref: "src/main.rs:35-37",
            status: RivetStatus::UnAttached,
            ..Default::default()
        };

        let git_origin = "git@gitlab.com:jake-jake-jake/rivet.git";
        let http_origin = "https://gitlab.com/jake-jake-jake/rivet.git";
        let expected = String::from("https://gitlab.com/jake-jake-jake/rivet/blob/master/src/main.rs#L35-L37");
        assert_eq!(r.make_link(git_origin), Ok(expected));
        let expected = String::from("https://gitlab.com/jake-jake-jake/rivet/blob/master/src/main.rs#L35-L37");
        assert_eq!(r.make_link(http_origin), Ok(expected));
        Ok(())
    }

    // test that rivets properly return errors for bad origins
    #[test]
    fn test_rivet_make_url_bad_origin() -> ioResult<()> {
        let r = Rivet {
            name: "A Link",
            doc_line: 2,
            code_ref: "./src/main.rs",
            status: RivetStatus::UnAttached,
            ..Default::default()
        };

        // test git
        let bad_git = "git@ we're doing bad here ";
        let bad_http = "http and it ain't so great";
        let just_bad = "this ain't parsable";
        assert_eq!(r.make_link(bad_git), Err("Bad origin url"));
        assert_eq!(r.make_link(bad_http), Err("Bad origin url"));
        assert_eq!(r.make_link(just_bad), Err("Bad origin url"));
        Ok(())
    }
    
    // test that rivets properly return errors for bad origins
    #[test]
    fn test_rivet_make_url_bad_ref() -> ioResult<()> {
        let r = Rivet {
            name: "A Link",
            doc_line: 2,
            code_ref: "this isn't so good",
            status: RivetStatus::UnAttached,
            ..Default::default()
        };
        let git_origin = "git@gitlab.com:jake-jake-jake/rivet.git";
        assert_eq!(r.make_link(git_origin), Err("Bad code ref"));
        Ok(())
    }
    
    // test that we properly get hash from a source code reference
    #[test]
    fn test_get_hash_from_ref() -> ioResult<()> {
        let target = hex::decode("b53a6c79c8e251e8192e5517f388cb16b6dfbabb").expect("Not a valid hash");
        let source_path = "tests/sample-sha-file";
        let path = env::current_dir()?;
        let repo = get_repository(path);
        let digest = get_hash_from_ref(source_path, &repo).unwrap();
        assert_eq!(target, digest);
        return Ok(());
    }
    
    // test that we properly get sha for lines of a file
    #[test]
    fn test_get_hash_lines() -> ioResult<()> {
        let target = hex::decode("3b0bb08a1379db5aa70b87cfac46ebc9d0217488").expect("Not a valid hash");
        let pb = PathBuf::from("./tests/sample-sha-file");
        let lines_ref = "1-5";
        let digest = get_line_hash(&pb, lines_ref).expect("Unable to get digest");
        assert_eq!(target, digest);

        let target = hex::decode("6585836c519fc09b76ade5a3c1d2e16c0be3026f").unwrap();
        let lines_ref = "1-4";
        let digest = get_line_hash(&pb, lines_ref).expect("Unable to get digest");
        assert_eq!(target, digest);

        let target = hex::decode("c1f08421776775baac9b2345894aa227fed0524f").unwrap();
        let lines_ref = "3";
        let digest = get_line_hash(&pb, lines_ref).expect("Unable to get digest");
        assert_eq!(target, digest);
        return Ok(());
    }

    // test rivet attaching
    #[test]
    fn test_rivet_attach() -> ioResult<()> {
        let path = env::current_dir()?;
        let repo = get_repository(path);
        let test_lines: Vec<(usize, &str)> = vec![(4, "[A link]: <./tests/sample-sha-file:3>")]; 
        let mut rivets = set_rivets(test_lines);
        for r in rivets.iter_mut() {
            if r.status == RivetStatus::UnAttached {
                r.attach(&repo);
            }
        }
        let e = Rivet {
            name: "A link",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3",
            hash: hex::decode("b53a6c79c8e251e8192e5517f388cb16b6dfbabb").unwrap(),
            line_hash: hex::decode("c1f08421776775baac9b2345894aa227fed0524f").unwrap(),
            status: RivetStatus::Attached
        };
        assert_eq!(rivets[0], e);

        let test_lines: Vec<(usize, &str)> = vec![(4, "[A bad ref]: <./tests/non-existant-file>")]; 
        let mut rivets = set_rivets(test_lines);
        for r in rivets.iter_mut() {
            if r.status == RivetStatus::UnAttached {
                r.attach(&repo);
            }
        }
        let e = Rivet {
            name: "A bad ref",
            doc_line: 4,
            code_ref: "./tests/non-existant-file",
            status: RivetStatus::Broken,
            ..Default::default()
        };
        assert_eq!(rivets[0], e);

        let test_lines: Vec<(usize, &str)> = vec![(4, "[An existing ref]: <./tests/sample-sha-file:3> (b53a6c79c8e251e8192e5517f388cb16b6dfbabb:c1f08421776775baac9b2345894aa227fed0524f)")]; 
        let mut rivets = set_rivets(test_lines);
        for r in rivets.iter_mut() {
            if r.status == RivetStatus::UnAttached {
                r.attach(&repo);
            }
        }
        let e = Rivet {
            name: "An existing ref",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3",
            hash: hex::decode("b53a6c79c8e251e8192e5517f388cb16b6dfbabb").unwrap(),
            line_hash: hex::decode("c1f08421776775baac9b2345894aa227fed0524f").unwrap(),
            status: RivetStatus::Attached
        };
        assert_eq!(rivets[0], e);

        let test_lines: Vec<(usize, &str)> = vec![(4, "[Bad existing]: <./tests/sample-sha-file:3> (deadbeef:line-hash)")]; 
        let mut rivets = set_rivets(test_lines);
        println!("{:?}", rivets);
        for r in rivets.iter_mut() {
            if r.status == RivetStatus::UnAttached {
                r.attach(&repo);
            }
        }
        println!("{:?}", rivets);
        let e = Rivet {
            name: "Bad existing",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3",
            status: RivetStatus::Broken,
            ..Default::default()
        };
        assert_eq!(rivets[0], e);
        
        return Ok(()); 
    }

    // test rivet checking
    #[test]
    fn test_rivet_check() -> ioResult<()> {
        let path = env::current_dir()?;
        let repo = get_repository(path);
        let r = Rivet {
            name: "A link",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3",
            hash: hex::decode("b53a6c79c8e251e8192e5517f388cb16b6dfbabb").unwrap(),
            line_hash: hex::decode("c1f08421776775baac9b2345894aa227fed0524f").unwrap(),
            status: RivetStatus::Attached
        };
        assert_eq!(r.check(&repo), Ok(true));
        
        let r = Rivet {
            name: "An existing ref",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3",
            hash: hex::decode("3a6c79c8e251e8192e5517f388cb16b6dfbabb").unwrap(),
            line_hash: hex::decode("f08421776775baac9b2345894aa227fed0524f").unwrap(),
            status: RivetStatus::Attached
        };
        assert_eq!(r.check(&repo), Ok(false));
        return Ok(());
    }

    // test rivet serialization
    #[test]
    fn test_rivet_serialization() -> ioResult<()> {
        let path = env::current_dir()?;
        let repo = get_repository(path);
        let origin = get_origin(&repo).unwrap();
        let mut r = Rivet {
            name: "A link",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3",
            hash: hex::decode("b53a6c79c8e251e8192e5517f388cb16b6dfbabb").unwrap(),
            line_hash: hex::decode("c1f08421776775baac9b2345894aa227fed0524f").unwrap(),
            status: RivetStatus::Attached
        };

        let dumped = r.dumps(&origin);
        println!("dumped 1: {}", dumped);
        r.code_ref = "https://gitlab.com/jake-jake-jake/rivet/blob/master/tests/sample-sha-file#L3";
        let r = r;
        let test_lines: Vec<(usize, &str)> = vec![(4, &dumped)];
        let rivets = set_rivets(test_lines);
        assert_eq!(rivets[0], r);

        let mut r = Rivet {
            name: "A link",
            doc_line: 4,
            code_ref: "./tests/sample-sha-file:3-5",
            hash: hex::decode("b53a6c79c8e251e8192e5517f388cb16b6dfbabb").unwrap(),
            line_hash: hex::decode("c1f08421776775baac9b2345894aa227fed0524f").unwrap(),
            status: RivetStatus::Attached
        };

        let dumped = r.dumps(&origin);
        r.code_ref = "https://gitlab.com/jake-jake-jake/rivet/blob/master/tests/sample-sha-file#L3-L5";
        let r = r;
        let test_lines: Vec<(usize, &str)> = vec![(4, &dumped)];
        let rivets = set_rivets(test_lines);
        assert_eq!(rivets[0], r);
        Ok(())
    }
}
