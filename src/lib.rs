use git2::Repository;
use hex;
use regex::Regex;
use sha1::{Sha1, Digest};
use std::path::{PathBuf, Path};
use std::{fs::File, vec::Vec};
use std::io::BufReader;
use std::io::prelude::*;
use walkdir::{WalkDir, DirEntry};

#[derive(Debug, Default, PartialEq)]
pub struct Rivet<'a> {
    pub name: &'a str,
    pub code_ref: &'a str,
    pub doc_line: usize,
    pub hash: Vec<u8>,
    pub line_hash: Vec<u8>,
    pub status: RivetStatus,
}

#[derive(Debug, PartialEq)]
pub enum RivetStatus {
    UnAttached,
    Attached,
    Broken
}

impl Default for RivetStatus {
    fn default() -> Self {RivetStatus::UnAttached}
}

impl<'a> Rivet<'a> {
    // produce a link parsed from a code reference (e.g., to the page on github, gitlab, etc.)
    pub fn make_link(&self, origin: &str) -> Result<String, &'static str> {
        // if rivet already has a link as a code ref, return that
        if self.code_ref.starts_with("http") {
            return Ok(String::from(self.code_ref));
        }

        // This can likely be made more elegant
        // Parse link route from origin url
        let git_re = Regex::new(r"git@(.*?):(.*?)\.git").unwrap();
        let http_re = Regex::new(r"https?://(.*?)\.git").unwrap();
        let mut link = String::from("https://");
        if origin.starts_with("git@"){
            let m = git_re.captures(origin);
            if m.is_none() {
                return Err("Bad origin url");
            }
            let groups = m.unwrap();
            link.push_str(groups.get(1).unwrap().as_str());
            link.push('/');
            link.push_str(groups.get(2).unwrap().as_str());
        } else if origin.starts_with("http") {
            let m = http_re.captures(origin);
            if m.is_none() {
                return Err("Bad origin url");
            }
            let groups = m.unwrap();
            link.push_str(groups.get(1).unwrap().as_str());
        } else {
            return Err("Bad origin url");
        }

        // Parse remaining piece of link from code ref
        let ref_re = Regex::new(r"^(?:./)?(.*?)$").unwrap();
        let m = ref_re.captures(self.code_ref);
        link.push_str("/blob/master/");
        if m.is_none() {
            return Err("Bad code ref")
        }
        let code_path = m.unwrap().get(1).unwrap().as_str();
        let mut in_line_numbers = false;
        for c in code_path.chars() {
            if c == ':' {
                link.push_str("#L");
                in_line_numbers = true;
            } else if in_line_numbers && c == '-' {
                link.push_str("-L");
            } else if c.is_whitespace() {
                return Err("Bad code ref")
            } else {
                link.push(c);
            }
        }
        return Ok(link);
    }

    // Attach rivet to repo.
    pub fn attach(&mut self, repo: &Repository) {
        if self.status != RivetStatus::UnAttached {
            return;
        }
        let (file_path, line_numbers) = parse_ref(&self.code_ref);
        let pb = PathBuf::from(file_path.clone());
        if line_numbers.is_some() {
            let line_numbers = line_numbers.unwrap();
            let res = get_line_hash(&pb, &line_numbers);
            if res.is_err() {
                self.status = RivetStatus::Broken;
                return;
            } else {
                self.line_hash = res.unwrap();
            }
        }

        let res = get_hash_from_ref(&file_path, repo);
        if res.is_err() {
            self.status = RivetStatus::Broken;
            return;
        } else {
            self.hash = res.unwrap();
        }

        self.status = RivetStatus::Attached;
    }

    pub fn check(&self, repo: &Repository) -> Result<bool, &'static str>{
        if self.status != RivetStatus::Attached {
            return Err("rivet is unattached");
        }
        let (file_path, line_numbers) = parse_ref(self.code_ref);

        let hash_check = get_hash_from_ref(&file_path, repo)?;
        let is_okay = self.hash == hash_check;
        if line_numbers.is_none() {
            return Ok(is_okay);
        } else {
            let line_numbers = line_numbers.unwrap();
            let hash_check = get_line_hash(&PathBuf::from(file_path), &line_numbers)?;
            let is_okay = hash_check == self.line_hash;
            return Ok(is_okay);
        }
    }

    pub fn dumps(&self, origin: &str) -> String {
        let link = match self.make_link(origin){
            Ok(x) => x,
            Err(_e) => format!("ERROR LINKING TO REPO: {}", self.code_ref)
        };
        let hash_digest = hex::encode(&self.hash);
        let line_digest = hex::encode(&self.line_hash);
        let comment = format!("{}:{}", hash_digest, line_digest);
        let s =  match self.status {
            RivetStatus::Attached => format!("[{}]: <{}> ({})",
                                             self.name,
                                             link,
                                             &comment),
            _ => format!("[{}]: <{}> (BROKEN RIVET)",
                         self.name,
                         link)
        };
        return s;
    }
}

// take rivet code ref and return path and line numbers
fn parse_ref(r: &str) -> (String, Option<String>) {
    let mut code_ref = String::from(r);
    if code_ref.starts_with("./") {
        code_ref.drain(..2);
    } else if code_ref.starts_with("http") {
        let p = "blob/master/";
        let idx = code_ref.find(p).unwrap_or(code_ref.len());
        code_ref.drain(..idx);
        code_ref.drain(..p.len());
    }

    let mut source_path = String::new();
    let mut line_numbers: Option<String> = None;
    if code_ref.contains(':') {
        let idx = code_ref.find(':').unwrap();
        source_path = code_ref.drain(..idx).collect();

        line_numbers = Some(code_ref.drain(1..).collect());
    } else if code_ref.contains('#') {
        // handle github style anchor links: `#L123-L125`
        let idx = code_ref.find('#').unwrap();
        source_path = code_ref.drain(..idx).collect();
        line_numbers = Some(code_ref.drain(1..).filter(|&c| c != 'L').collect());
    }
    else {
        source_path = code_ref.drain(..).collect();
    }
    return (source_path, line_numbers);
}

// get the hash for a given file from a git repository
pub fn get_hash_from_ref(source_path: &str,
                         repo: &Repository) -> Result<Vec<u8>, &'static str> {
    let rev_spec = format!("master:{}", source_path);
    let object = match repo.revparse_single(&rev_spec) {
        Ok(object) => object,
        _ => return Err("Could not find file in repo.")
    };
    return Ok(object.id().as_bytes().to_vec());
}

// get sha1 hash of lines from file at path
pub fn get_line_hash(path: &PathBuf, line_ref: &str) -> Result<Vec<u8>, &'static str> {
    let line_numbers: Vec<usize> = line_ref.split('-')
        .map(|i| i.parse::<usize>().expect("Cannot parse provided line number"))
        .map(|i| i - 1).collect();
    let start = line_numbers[0];
    let end = line_numbers[line_numbers.len() - 1];
    let file = File::open(&path).expect("Could not open file");

    // hash appropriate lines
    let mut sha = Sha1::new(); 
    for (i, l) in BufReader::new(file).lines().enumerate() {
        if i < start {
            continue;
        } else if i > end {
            break;
        }
        sha.input(l.unwrap().as_bytes());
        sha.input("\n".as_bytes());
    }
    let line_hash: Vec<u8> = sha.result().to_vec();
    return Ok(line_hash);
}

pub fn get_repository(path: PathBuf) -> Repository {
    let repo = match Repository::open(path) {
        Ok(repo) => repo,
        Err(e) => panic!("No repository in current working directory: {}", e)
    };
    return repo;
}

pub fn get_origin(repo: &Repository) -> Result<String, &'static str> {
    let remote = match repo.find_remote("origin") {
        Ok(x) => String::from(x.url().unwrap_or_default()),
        Err(_e) => String::from("Unable to discover origin") 
    };
    return Ok(remote);
}

fn is_doc_type(entry: &DirEntry) -> bool {
    // lazy_static! {
    //     static ref RE: Regex = Regex::new(r"\.md$").unwrap();
    // }
    let p = Regex::new(r"\.md$").unwrap();
    let file_name = entry.file_name().to_str().unwrap();
    return p.is_match(file_name);
}

// search doc for places marked for attaching rivets
pub fn get_rivet_holes(contents: &str) -> Vec<(usize, &str)> {
    let mut is_rivet = false;
    let mut rivet_holes: Vec<(usize, &str)> = Vec::new();
    for (i, line) in contents.lines().enumerate() {
        if line.starts_with("<!-- RIVETS") {
            is_rivet = true;
            continue;
        }
        if is_rivet {
            if line.starts_with("[") {
                rivet_holes.push((i, line));
            }
            else {
                is_rivet = false;
            }
        }
    }
    return rivet_holes;
}

// set new rivets in holes: create a Rivet for each ref in a doc, copying metadata from existing ones
pub fn set_rivets(rivet_holes: Vec<(usize, &str)>) -> Vec<Rivet> {
    // lazy_static! {
    //     static ref NEW_RIVET_P: Regex = Regex::new(r"(?x)
    //          \[(?P<name>.*?)\]:\s # name of the link
    //          <(?P<ref>.*?)>\s?    # ref for the link
    //          (\((?P<comment>.+?)\))?").unwrap();
    //     static ref EXISTING_RIVET_P: Regex = Regex::new(r"\{.*?\}").unwrap();
    // }
    let mut attached: Vec<Rivet> = Vec::new();
    for (i, line) in rivet_holes {
        let res = parse_rivet_line(i, line);
        
        if res.is_err() {
            println!("Error parsing rivet line: {}! {:?}", line, res);
            continue;
        } else {
            attached.push(res.unwrap());
        }
    }
    return attached;
}

fn parse_rivet_line(ln: usize, line: &str) -> Result<Rivet, &'static str> {
    let rivet_p: Regex = Regex::new(r"\[(?P<name>.*?)\]: <(?P<ref>.*?)>( \((?P<hash>([^)]+))\))?").unwrap();

    let m = rivet_p.captures(line);
    if m.is_none() {
        return Err("bad rivet line");
    }
    let m = m.unwrap();
    let name = m.name("name").expect("unable to find name").as_str();
    let code_ref = m.name("ref").expect("unable to find ref").as_str();

    // parse existing rivet
    if let Some(h) = m.name("hash") {
        let hashes = h.as_str();
        let existing: Vec<Result<Vec<u8>, _>> = hashes.split(':')
            .map(|i| hex::decode(i))
            .collect();

        if existing.len() < 2 {
            let r = Rivet {
                name: name,
                code_ref: code_ref,
                doc_line: ln,
                status: RivetStatus::Broken,
                ..Default::default()
            };
            return Ok(r)
        }

        let hash_res = existing[0].clone();
        let line_hash_res = existing[1].clone();
        if hash_res.is_err() || line_hash_res.is_err() {
            let r = Rivet {
                name: name,
                code_ref: code_ref,
                doc_line: ln,
                status: RivetStatus::Broken,
                ..Default::default()
            };
            return Ok(r)
        } else {
            let r = Rivet {
                name: name,
                code_ref: code_ref,
                doc_line: ln,
                hash: hash_res.unwrap(),
                line_hash: line_hash_res.unwrap(),
                status: RivetStatus::Attached
            };
            return Ok(r);
        }
    }
    // create new rivet
    let r = Rivet {
        name: name,
        code_ref: code_ref,
        doc_line: ln,
        hash: vec!(),
        line_hash: vec!(),
        status: RivetStatus::UnAttached
    };
    return Ok(r)

}

pub fn get_doc_files(path: &Path) -> Vec<DirEntry> {
    let mut docs: Vec<DirEntry> = Vec::new();
    for entry in WalkDir::new(path)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| is_doc_type(&e)){
            docs.push(entry)
    }
    return docs
}

#[cfg(test)]
mod tests {
    use std::io::Result as ioResult;
    use super::*;

    // assume that cargo is testing from the root directory of the package
    #[test]
    fn test_parse_ref() -> ioResult<()>  {
        let url = "https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L147-L150";
        let (r, lr) = parse_ref(&url);
        assert_eq!(r, "src/lib.rs");
        assert_eq!(lr, Some(String::from("147-150")));
        Ok(())
    }
}
