mod lib;
use std::{collections::HashMap, env};
use std::fs;
use std::io::BufWriter;
use std::io::prelude::*;

// fetch, attach, and check rivets
fn main() -> std::io::Result<()> {
    let cwd = env::current_dir()?;
    let repo = lib::get_repository(cwd);
    let cwd = env::current_dir()?;
    let doc_files = lib::get_doc_files(&cwd);
    let origin = lib::get_origin(&repo).unwrap();

    for fd in doc_files.iter() {
        println!("Adding rivets to {:?}", fd);
        let contents = fs::read_to_string(fd.path()).expect("Unable to open file");
        let rivet_holes = lib::get_rivet_holes(&contents);
        let mut rivets = lib::set_rivets(rivet_holes);
        for r in rivets.iter_mut() {
            r.attach(&repo);
        }
        let mut rivet_map: HashMap<usize, &lib::Rivet> = HashMap::new();
        for r in rivets.iter() {
            rivet_map.insert(r.doc_line, &r);
        }

        let file = fs::OpenOptions::new().write(true).truncate(true).open(fd.path())?;
        let mut writer = BufWriter::new(&file);
        for (i, l) in contents.lines().enumerate() {
            if rivet_map.contains_key(&i) {
                let r = rivet_map.get(&i).unwrap();
                writer.write(r.dumps(&origin).as_bytes()).unwrap();
                writer.write("\n".as_bytes()).unwrap();
            } else {
                writer.write(l.as_bytes()).unwrap();
                writer.write("\n".as_bytes()).unwrap();
            }
        }

    }

    for fd in doc_files.iter() {
        println!("Checking rivets...");
        let contents = fs::read_to_string(fd.path()).expect("Unable to open file");
        let rivet_holes = lib::get_rivet_holes(&contents);
        let mut rivets = lib::set_rivets(rivet_holes);
        for r in rivets.iter_mut() {
            let res = r.check(&repo);
            if res.is_err() {
                println!("rivet is broken: {:?}", r);
            }
        }

    }
    return Ok(());
}
