<!-- RIVETS -->
[set_rivets]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L297> (bb43f644fa0e898764b00a99942fd1ca967654d8:a4d79fc6c885f77ec8c7c41c064f51d16b9476fb)
[make_links]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L32> (bb43f644fa0e898764b00a99942fd1ca967654d8:cfd2d68631aad94bd4e821e64cecbb2f22a3a0e3)
[check]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L147-L150> (bb43f644fa0e898764b00a99942fd1ca967654d8:a64a318937ed11ea79ca807beba9225a72a69c86)

# rivet
`rivet` attaches documentation to source code.

## Easy links to your online repos
Prepend a section of named links to your markdown documentation with an HTML comment that says `RIVETS`:

```
<!-- RIVETS -->
[set_rivets]: <./src/lib.rs:297> 
[make_links]: <./src/lib.rs:32>
[check]: <./src/lib.rs:147-150>
```

`rivet` finds these references and [pins them][set_rivets] to the current version of source,
producing smart links to the code as it exists, like so:"

```
<!-- RIVETS -->
[set_rivets]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L297> (b98bd6e062827d4722e0118306587e12220a7454:42fc6ce3acd639fbf4f9c50ed2790f9ad3e38fed)
[make_links]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L32> (b98bd6e062827d4722e0118306587e12220a7454:18e7bae0abf5f802ffc75bea5577675f913bba9f)
[check]: <https://gitlab.com/jake-jake-jake/rivet/blob/master/src/lib.rs#L147-L150> (b98bd6e062827d4722e0118306587e12220a7454:d6994dfed743d3e131b24480f2b7b715bfcdda01)
```

`rivet` walks a repo's directory and processes all documentation files, [generating links][make_links] 
and letting you know when your docs need to be updated, by [checking rivets][check] against version control. 
